﻿using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models.Entities
{
    public class Produto
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        [Required]
        public decimal Value { get; set; }
        public int Amount { get; set; }

        public int VendaId { get; set; }
    }
}
