﻿using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models.Entities
{
    public class Venda
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; } = new Vendedor();
        
        public List<Produto> Produtos { get; set; } = new List<Produto>();
        
        public EnumStatusVenda Status { get; set; } = EnumStatusVenda.AguardandoPagamento;
        
        public DateTime DataDaVenda { get; set; } = DateTime.Now;

    }
}
