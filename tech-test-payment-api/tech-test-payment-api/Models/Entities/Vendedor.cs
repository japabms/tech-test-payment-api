﻿using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models.Entities
{
    public class Vendedor
    {
        public int Id { get; set; }
        [Required]
        public string Cpf { get; set; } = string.Empty;
        public string Nome { get; set; } = string.Empty;
        public string? Email { get; set; }
        public string Telefone { get; set; } = string.Empty;
    }
}
