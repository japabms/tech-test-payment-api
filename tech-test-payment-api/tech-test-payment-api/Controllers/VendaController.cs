﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System.ComponentModel;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly LojaContext _context;

        public VendaController(LojaContext context) 
        {   
            _context = context;
        }

        [HttpPost]
        public IActionResult Create(Venda venda)
        {
            _context.Vendas.Add(venda);
            _context.SaveChanges();

            return Ok();
        }

        [HttpPut]
        public IActionResult Update(int id, EnumStatusVenda status)
        {
            var vendaNoDb = _context.Vendas.Find(id);

            if (vendaNoDb == null) return NotFound();

            if (vendaNoDb.Status == EnumStatusVenda.AguardandoPagamento
                && status == EnumStatusVenda.PagamentoAprovado
                || vendaNoDb.Status == EnumStatusVenda.AguardandoPagamento
                && status == EnumStatusVenda.Cancelado)
            {
                vendaNoDb.Status = status;
            }
            else if (vendaNoDb.Status == EnumStatusVenda.PagamentoAprovado 
                && status == EnumStatusVenda.EnviadoParaTransportadora
                || vendaNoDb.Status == EnumStatusVenda.PagamentoAprovado
                && status == EnumStatusVenda.Cancelado)
            {
                vendaNoDb.Status = status;
            }
            else if (vendaNoDb.Status == EnumStatusVenda.EnviadoParaTransportadora && status == EnumStatusVenda.Entregue)
            {
                vendaNoDb.Status = status;
            }
            else
            {
                return BadRequest();
            }
           


            _context.Vendas.Update(vendaNoDb); 
            _context.SaveChanges();

            return Ok();
        }



        [HttpGet]
        public IActionResult GetById(int id)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null) return NotFound();

            var vendedor = _context.Vendedores.Find(venda.VendedorId);

            if (vendedor == null) return NotFound();

            var produtos = _context.Produtos.Where(x => x.Id == id).ToList();

            var v = new Venda()
            {
                DataDaVenda = venda.DataDaVenda,
                Status = venda.Status,
                Vendedor = new Vendedor()
                {
                    Nome = vendedor.Nome,
                    Cpf = vendedor.Cpf,
                    Email = vendedor.Email,
                    Telefone = vendedor.Telefone,
                }
            };

            foreach (var item in produtos)
            {
                v.Produtos.Add(new Produto
                {
                    Id = item.Id,
                    Amount = item.Amount,
                    Value = item.Value,
                    Name = item.Name,
                    Description = item.Description
                });
            }

            return Ok(v);
        }



    }
}
